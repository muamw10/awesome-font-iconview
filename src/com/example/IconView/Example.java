package com.example.IconView;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;

public class Example extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid);

        GridView grid = (GridView) findViewById(R.id.gridview);
        grid.setAdapter(ArrayAdapter.createFromResource(this, R.array.icons, R.layout.icon));
    }
}
